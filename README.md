## Taxi App Theme

Thanks for purchasing the Taxi App Theme.

Follow the documentation to install and get started with the development:

-   [Documentation](http://strapmobile.com/docs/react-native-uber-like-app-script/v4.0.0)
-   [Product Page](http://strapmobile.com/react-native-uber-like-app-script/)

Please do check `FAQs` section in docs for queries.

Happy coding!
